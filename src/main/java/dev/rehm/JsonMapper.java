package dev.rehm;

import dev.rehm.annotations.JsonIgnore;
import dev.rehm.annotations.JsonKey;
import dev.rehm.exceptions.JsonMappingException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Locale;

/*
    new Actor(2, "Ken Watanabe", LocalDate.of(1985,2,3)) -> "{ "id":2 , "name":"Ken Watanabe", "birthday":"1985-2-3" }"

    "{ "id":2 , "name":"Ken Watanabe", "birthday":"1985-2-3" }" -> new Actor(2, "Ken Watanabe", LocalDate.of(1985,2,3))
 */
public class JsonMapper implements Mapper{


    /*
        this method will convert any object to its json representation
     */
    @Override
    public String serialize(Object o) {
        // obtain the class information from o
        Class<?> objectClass = o.getClass();

        // iterate through each instance variable, or field --- Fields [id, name, birthday]
        Field[] fields = objectClass.getDeclaredFields();

        String partialJson = "";

        for(Field field: fields){
            if(!field.isAnnotationPresent(JsonIgnore.class)){
                partialJson += convertFieldToJsonKeyValueString(field, o);
            }
        }

        return "{" +partialJson.substring(0,partialJson.length()-1) +" }" ;
    }

    private String convertFieldToJsonKeyValueString(Field field, Object o){
        String fieldName = field.getName();

        // by default, the JSON key name will be the same as the field name
        String keyName = fieldName;
        // if there is a JsonKey annotation, we will set the name value from the annotation to the json key value
        JsonKey jsonKey = field.getAnnotation(JsonKey.class);
        if(jsonKey!=null){
            keyName = jsonKey.name();
        }

        String getterName = "get"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
        try {
            //  access the getter directly based on the field name
            Method getterMethod = o.getClass().getMethod(getterName);

            // if we invoke the getter, we get the field value
            Object fieldValue = getterMethod.invoke(o);

            // we can use each field name and value to create a json string
            // we could make this better by not wrapping our numeric types in quotations
            return " \""+keyName+"\" : \""+fieldValue.toString()+"\",";
        } catch (NoSuchMethodException e) {
            throw new JsonMappingException("No suitable getter for: "+fieldName, e);
        } catch (IllegalAccessException e) {
            throw new JsonMappingException("Cannot access getter for: "+fieldName, e);
        } catch (InvocationTargetException e) {
            throw new JsonMappingException("Issue invoking getter for: "+fieldName, e);
        }
    }

    /*
       converts a string to an object
       { "id" : "2", "name" : "Ken Watanabe", "birthday" : "1959-10-21" }
     */
    @Override
    public <T> T deSerialize(String input, Class<T> clazz) {
        if(input==null || input.equals("")){
            return null;
        }
        String partialJson = input.substring(1,input.length()-1);
        String[] keyValueStrings = partialJson.split(",");
        T newObject = null;

        try {
            newObject = clazz.newInstance();
        } catch (InstantiationException |IllegalAccessException e) {
            e.printStackTrace();
        }

        for(String s: keyValueStrings){
            String[] keyValueArr = s.split(":");
            if(keyValueArr.length != 2){
                throw new JsonMappingException("Improperly formatted JSON");
            }
            String keyString = keyValueArr[0].trim();
            keyString = keyString.substring(1, keyString.length()-1);
            String valueString = keyValueArr[1].trim();
            valueString = valueString.substring(1, valueString.length()-1);
//            System.out.println("key string: "+keyString + "   - value string: "+valueString );

            //obtain each setter method we need to set the values
            String setterName = "set"+keyString.substring(0,1).toUpperCase() + keyString.substring(1);
            try {
                // getting the type of the setter parameter, based on the field type
                Class<?> setterParamType = clazz.getDeclaredField(keyString).getType();

                // obtain the setter method using the setter name and setter paramter type
                Method setter = clazz.getMethod(setterName, setterParamType);

                Object fieldValue = convertStringToFieldType(valueString, setterParamType);

                setter.invoke(newObject,fieldValue);
            } catch (NoSuchFieldException  e) {
                throw new JsonMappingException(keyString+" field does not exist in class "+clazz);
            } catch ( NoSuchMethodException e){
                throw new JsonMappingException("no valid setter for: "+keyString);
            } catch (IllegalAccessException e) {
                throw new JsonMappingException("cannot access setter for: "+keyString);
            } catch (InvocationTargetException | InstantiationException e) {
                throw new JsonMappingException("issue invoking setter for: "+keyString);
            }

        }
        return newObject;
    }

    private Object convertStringToFieldType(String input, Class<?> type) throws IllegalAccessException, InstantiationException {
        switch(type.getName()){
            case "byte":
                return Byte.valueOf(input);
            case "short":
                return Short.valueOf(input);
            case "int":
                return Integer.valueOf(input);
            case "long":
                return Long.valueOf(input);
            case "boolean":
                return Boolean.valueOf(input);
            case "java.lang.String":
                return input;
            case "java.time.LocalDate":
                return LocalDate.parse(input);
            default:
                return type.newInstance();
        }
    }
}
