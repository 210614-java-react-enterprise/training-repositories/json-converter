package dev.rehm;

import dev.rehm.models.Actor;

import java.time.LocalDate;

public class JsonApp {

    public static void main(String[] args) {
        JsonMapper jsonMapper = new JsonMapper();
        Actor actor = new Actor(2, "Ken Watanabe", LocalDate.of(1959,10,21));
        System.out.println(jsonMapper.serialize(actor));

//        String actorJson = "{ \"id\" : \"2\", \"name\" : \"Ken Watanabe\", \"birthday\" : \"1959-10-21\" }";
//        Actor deserializedObject = jsonMapper.deSerialize(actorJson, Actor.class);
//        System.out.println(deserializedObject);
    }
}
